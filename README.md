mailq
=====

Web application to display a mail queue distributed on a mailhub cluster

mailq is a tool to put mailqueue statistics into
a database and to view it with a web interface.

The web interface part depends on htg
	https://github.com/pdav/netmagis/tree/master/www/htg

mailq was written by Pierre David (pdagog@gmail.com)

