#
# Makefile pour lancer l'installation de l'application mailq
#

DBHOST = localhost
DBNAME = mailq
DBUSER = mailq
DBPASSWORD = secret1234

TCLSH	= /usr/bin/tclsh
DESTDIR	= /local/applis/mailq
PKGTCL	= /local/lib
BASE    = {host=$(DBHOST) dbname=$(DBNAME) user=$(DBUSER) password=$(DBPASSWORD)}

#
# Motif de substitution à appliquer à chaque fichier installé
#

SUBST	= \
	-e 's|%TCLSH%|$(TCLSH)|' \
	-e 's|%DESTDIR%|$(DESTDIR)|' \
	-e 's|%PKGTCL%|$(PKGTCL)|' \
	-e 's|%BASE%|$(BASE)|'

#
# Les cibles
#

all: pre dirs files exec

pre:
	rm -rf $(DESTDIR)/*

dirs:
	if [ ! -d $(DESTDIR) ] ; then mkdir -p $(DESTDIR) ; fi
	find . -type d -print | \
	    while read d ; do \
		if [ ! -d $(DESTDIR)/$$d ] ; then mkdir -p $(DESTDIR)/$$d ; fi ; \
	    done

files:
	find . \( -type f -o -type l \) -print | \
	    grep -v '.htgt$$' | \
	    grep -v '.gif$$' | \
	    grep -v Makefile | \
	    while read f ; do \
		echo $$f ; \
		sed $(SUBST) $$f > $(DESTDIR)/$$f ; \
	    done

exec:
	chmod +x $(DESTDIR)/bin/*
