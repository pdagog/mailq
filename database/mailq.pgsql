--
-- Creation de la base
--

CREATE TABLE adresses (
    hostid integer NOT NULL,
    id text NOT NULL,
    status integer NOT NULL,
    localpart text,
    remotepart text
);

CREATE TABLE erreurs (
    typeerr integer NOT NULL,
    texte text,
    regexp text
);

CREATE TABLE hosts (
    hostid integer NOT NULL,
    mailhostname text,
    hostname text
);

CREATE TABLE messages (
    hostid integer NOT NULL,
    id text NOT NULL,
    creation integer,
    message text,
    servsmtp text,
    servaddr inet,
    typeerr integer,
    clientname text,
    clientaddr inet,
    clientforged integer
);

CREATE TABLE stats (
    hostid integer NOT NULL,
    date timestamp with time zone NOT NULL,
    insert integer,
    update integer,
    delete integer,
    rien integer,
    elapsed interval
);

COPY erreurs (typeerr, texte, regexp) FROM stdin;
0	Pas d'erreur	
1	Connection refused	Deferred: Connection refused by (.*).
2	Operation timed out	Deferred: Operation timed out with (.*).
3	Connection reset	Deferred: Connection reset by (.*).
4	No route to host	Deferred: ([^ ]*).: No route to host
5	Host name lookup failure	Deferred: Name server: (.*): host name lookup failure
6	Host name lookup failure	host map: lookup .(.*).: deferred
9999	Other	.*
7	User unknown	Deferred: 450 <[^@]+@([^>]+)>:.*User unknown.*
8	Read error	.*read error from (.*).
9	Write error	.*writing message to ([^:]+).: .*
10	Host is down	.*Host ([^ ]+) is down
11	Rejected	host ([^ ]+) refused to talk to me:.*
12	Rejected	host ([^ ]+) said:.*
13	Connection refused	connect to ([^ ]+): Connection refused
14	Operation timed out	connect to ([^ ]+): Connection timed out
15	No route to host	connect to ([^ ]+): Network is unreachable
16	Operation timed out	conversation with ([^ ]+): timed out while receiving the initial server greeting
17	Connection reset	lost connection with ([^ ]+) receiving the initial server greeting
18	Connection refused	delivery temporarily suspended: connect to ([^ ]+): Connection refused
19	Operation timed out	delivery temporarily suspended: connect to ([^ ]+): Connection timed out
20	Connection reset	delivery temporarily suspended: lost connection with ([^ ]+) while receiving the initial server greeting
\.

COPY hosts (hostid, mailhostname, hostname) FROM stdin;
1	mailhost - 1e machine du cluster	mr1.u-strasbg.fr
2	mailhost - 2e machine du cluster	mr2.u-strasbg.fr
3	mailhost - 3e machine du cluster	mr3.u-strasbg.fr
4	mailhost - 4e machine du cluster	mr4.u-strasbg.fr
5	mailhost - 5e machine du cluster	mr5.u-strasbg.fr
6	mailhost - 6e machine du cluster	mr6.u-strasbg.fr
7	mailhost - 7e machine du cluster	mr7.u-strasbg.fr
8	mailhost - 8e machine du cluster	mr8.u-strasbg.fr
9	mailhost - 9e machine du cluster	mr9.u-strasbg.fr
\.

ALTER TABLE ONLY adresses
    ADD CONSTRAINT adresses_pkey PRIMARY KEY (hostid, id, status);

ALTER TABLE ONLY erreurs
    ADD CONSTRAINT erreurs_pkey PRIMARY KEY (typeerr);

ALTER TABLE ONLY hosts
    ADD CONSTRAINT hosts_hostname_key UNIQUE (hostname);

ALTER TABLE ONLY hosts
    ADD CONSTRAINT hosts_pkey PRIMARY KEY (hostid);

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (hostid, id);

ALTER TABLE ONLY stats
    ADD CONSTRAINT stats_pkey PRIMARY KEY (hostid, date);

ALTER TABLE ONLY messages
    ADD CONSTRAINT "$1" FOREIGN KEY (hostid) REFERENCES hosts(hostid);

ALTER TABLE ONLY adresses
    ADD CONSTRAINT "$1" FOREIGN KEY (hostid) REFERENCES hosts(hostid);

ALTER TABLE ONLY stats
    ADD CONSTRAINT "$1" FOREIGN KEY (hostid) REFERENCES hosts(hostid);
