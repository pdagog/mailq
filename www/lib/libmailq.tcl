#
# Librairie TCL pour l'application de gestion de mailq
#
# Historique
#   2002/02/05 : pda     : conception
#

##############################################################################
# Sortie des erreurs
##############################################################################

#
# Sortie des erreurs dans une belle page Web
#
# Entrée :
#   - paramètres :
#	- page : fichier contenant la page HTML à trous
#	- msg : le message d'erreur
# Sortie : pas de sortie, la procédure fait un exit.
#
# Historique
#   2000/07/26 : pda     : conception
#   2000/07/27 : pda     : documentation
#   2001/10/20 : pda     : utilisation de la procédure de sortie
#

proc erreur {page msg} {
    ::webapp::send html [::webapp::file-subst $page \
					[list [list %MESSAGE% $msg]] \
				]
    exit 0
}

#
# Sortie des erreurs relatives aux formulaires
#
# Entrée :
#   - paramètres :
#	- page : fichier contenant la page HTML à trous
#	- ftab : le tableau contenant le message d'erreur
# Sortie : pas de sortie, la procédure fait un exit.
#
# Historique
#   2001/10/20 : pda     : conception
#

proc erreur-formulaire {page ftab} {
    upvar $ftab t

    erreur $page "Formulaire non conforme aux spécifications&nbsp: $t(_error)"
}

##############################################################################
# Noms des mailhosts et dates de rafraichissement
##############################################################################

set libconf(mailhosts)	{
    global {
	chars {12 normal}
	columns {60 40}
    }
    pattern {Titre} {
	title {yes}
	topbar {yes}
	chars {bold}
	align {center}
	vbar {yes}
	botbar {yes}
	column { }
	vbar {yes}
	column { }
	vbar {yes}
    }
    pattern {Normal} {
	botbar {yes}
	align {left}
	vbar {yes}
	column { }
	vbar {yes}
	column { }
	vbar {yes}
    }
}


#
# Retourne le code HTML contenant le nom des mailhosts et la date
# de dernier rafraichissement.
#
# Entrée :
#   - paramètres :
#	- base : informations de connexion à la base contenant les tickets
#   - variables globales :
#	- libconf(mailhosts) : spécification de tableau pour les mailhosts
# Sortie :
#   - valeur de retour : code HTML
#
# Historique
#   2003/04/22 : pda     : conception
#

proc noms-mailhosts {dbfd} {
    global libconf

    set donnees {}
    lappend donnees {Titre Serveur {Dernière mise à jour}}
    set sql "SELECT hostid, mailhostname FROM hosts ORDER BY mailhostname"
    pg_select $dbfd $sql t1 {
	set hostid   $t1(hostid)
	set hostname $t1(mailhostname)
	set sql "SELECT date, to_char (date, 'HH24:MI:SS DD MONTH YYYY') AS d
				FROM stats
				WHERE hostid = $hostid
				ORDER BY date DESC
				LIMIT 1"
	set date "(pas de mise à jour)"
	pg_select $dbfd $sql t2 {
	    set date [clock format \
				[clock scan $t2(d)] \
				-format "%a %d %b %H:%M:%S" \
			    ]
	}
	lappend donnees [list Normal $hostname $date]
    }
    return [::arrgen::output "html" $libconf(mailhosts) $donnees]
}

##############################################################################
# Accès à la base
##############################################################################

#
# Initie l'accès à la base
#
# Entrée :
#   - paramètres :
#	- base : informations de connexion à la base contenant les tickets
#	- varmsg : message d'erreur lors de l'écriture, si besoin
#   - variables globales :
#	- debug(base) : si la variable existe, elle doit contenir le
#		nom d'une base qui sera utilisée pour tous les accès
# Sortie :
#   - valeur de retour : accès à la base
#
# Historique
#   2001/01/27 : pda     : conception
#   2001/10/09 : pda     : utilisation de conninfo pour accès via passwd
#

proc ouvrir-base {base varmsg} {
    upvar $varmsg msg
    global debug

    #
    # On ne sait jamais... Intercepter la lecture éventuellement.
    #

    if {[info exists debug(base)]} then {
	set base $debug(base)
    }

    if {[catch {set dbfd [pg_connect -conninfo $base]} msg]} then {
	return ""
    }

    #
    # Changer le format des dates pour quelque chose de compatible
    # avec le format "clock scan/clock format"
    #

    set sql "SET DATESTYLE TO ISO,US"
    if {! [::pgsql::execsql $dbfd $sql msg]} then {
        pg_disconnect $dbfd
        return ""
    }

    return $dbfd
}

#
# Clôt l'accès à la base
#
# Entrée :
#   - paramètres :
#	- dbfd : accès à la base
# Sortie :
#   - valeur de retour : aucune
#
# Historique
#   2001/01/27 : pda     : conception
#

proc fermer-base {dbfd} {
    pg_disconnect $dbfd
}
