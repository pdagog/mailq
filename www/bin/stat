#!%TCLSH%

#
# Script pour donner un aperçu de la mailq
#
# Appelé par : page d'index
#
# Paramètres (formulaire ou URL) : aucun
#
# Historique
#   2002/02/06 : pda     : création
#

set conf(homeurl)	%HOMEURL%

#
# Chemins utilisés par les scripts
#

set conf(pkg)		%PKGTCL%
set conf(lib)		%DESTDIR%/lib
set conf(libmailq)	$conf(lib)/libmailq.tcl

#
# Définition des noms des pages "à trous"
#

set conf(page)		$conf(lib)/stat.html
set conf(err)		$conf(lib)/erreur.html

set conf(pageraisons)	$conf(homeurl)/raisons.html

#
# Quelques paramètres du script
#

set conf(base)		%BASE%

# type d'erreur correspondant aux messages non reconnus dans la base
set conf(othermsg)	9999

set conf(tableau)	{
    global {
	chars {12 normal}
	columns {80 20}
    }
    pattern {Titre} {
	title {yes}
	topbar {yes}
	chars {bold}
	align {center}
	botbar {yes}
	vbar {yes}
	column { }
	vbar {yes}
	column { }
	vbar {yes}
    }
    pattern {Normal} {
	botbar {yes}
	vbar {yes}
	column {
	    format {raw}
	    align {left}
	}
	vbar {yes}
	column {
	    align {right}
	}
	vbar {yes}
    }
    pattern {Gras} {
	chars {bold}
	botbar {yes}
	vbar {yes}
	column {
	    align {left}
	}
	vbar {yes}
	column {
	    align {right}
	}
	vbar {yes}
    }
}

#
# Les outils du parfait concepteur de pages Web dynamiques...
#

lappend auto_path $conf(pkg)
package require webapp
package require pgsql
package require arrgen

#
# On y va !
#

# ::webapp::cgidebug ; exit

source $conf(libmailq)

##############################################################################
# Programme principal
##############################################################################

proc main {} {
    global conf

    #
    # Accès à la base
    #

    set dbfd [ouvrir-base $conf(base) msg]
    if {[string length $dbfd] == 0} then { erreur $conf(err) $msg }

    #
    # Lecture de la date du dernier remplissage de mqueue
    #

    set mailhosts [noms-mailhosts $dbfd]

    #
    # Lecture des types d'erreur
    #

    pg_select $dbfd "SELECT typeerr, texte FROM erreurs" tab {
	set tabtypes($tab(typeerr)) [::webapp::html-string $tab(texte)]
    }

    #
    # Extraire des stats par type d'erreur
    #

    set total 0
    set donnees ""
    lappend donnees [list Titre {Message d'erreur} {Nombre de messages}]

    set sql "SELECT m.typeerr, e.texte, count(*) \
			FROM messages m, erreurs e \
			WHERE m.typeerr = e.typeerr \
			    AND e.typeerr <> $conf(othermsg) \
			GROUP BY m.typeerr, e.texte \
			ORDER BY m.typeerr ASC"

    pg_select $dbfd $sql tab {
	set typeerr	$tab(typeerr)
	set txt		$tab(texte)
	set nb		$tab(count)

	set urlmsg "<A HREF=\"$conf(pageraisons)#$typeerr\">$txt</A>"

	lappend donnees [list Normal $urlmsg $nb]

	incr total $nb
    }

    set typeerr	$conf(othermsg)
    set sql "SELECT message, count(*) \
			FROM messages \
			WHERE typeerr = $typeerr \
			GROUP BY message
			ORDER BY message ASC"
    pg_select $dbfd $sql tab {
	set txt		$tab(message)
	set nb		$tab(count)

	set urlmsg "<A HREF=\"$conf(pageraisons)#$typeerr\">$txt</A>"
	lappend donnees [list Normal $urlmsg $nb]

	incr total $nb
    }
    lappend donnees [list Gras Total $total]

    #
    # Présenter les résultats dans un zoli tableau
    #

    set tableau [::arrgen::output "html" $conf(tableau) $donnees]

    ::webapp::send html [::webapp::file-subst $conf(page) \
				[list \
				    [list %MAILHOSTS% $mailhosts] \
				    [list %TABLEAU% $tableau] \
				    ] \
				] \

    #
    # Déconnexion de la base
    #

    fermer-base $dbfd
}

main
exit 0
